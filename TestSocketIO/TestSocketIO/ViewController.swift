//
//  ViewController.swift
//  TestSocketIO
//
//  Created by macmini on 5/7/17.
//  Copyright © 2017 macmini. All rights reserved.
//

import UIKit

import SocketIO
import CoreData

import  UserNotifications
import UserNotificationsUI

class ViewController: UIViewController {
    @IBOutlet weak var lblNumber: UILabel!
    
    
    var numbers: [NSManagedObject] = []

    
    let requestIdentifier = "SampleRequest"
    
    //var socket: SocketIOClient?
    var numbersArray=NSMutableArray()
    var DBArray=NSMutableArray()

    
    let socket = SocketIOClient(socketURL: URL(string: "http://ios-test.us-east-1.elasticbeanstalk.com")!, config: [.nsp("/random"), .log(true), .forcePolling(true), .forceWebsockets(true)])


    override func viewDidLoad() {
        
        self.title = "Home"
        
        socket.on("connect") {data, ack in
            print("socket connected")
            self.lblNumber.text="Socket connected"
            
        }
        
        socket.on("capture") {data, ack in
            print(" capture ",data[0])
            
            if(self.numbersArray.contains(data[0]))
            {
                //Need to fire Local notification
                self.triggerNotification(strNum: data[0] as! NSNumber)
                print("Fire Local notifications");
            }else
            {
                self.numbersArray.insert(data[0], at: 0)
            }
            
            // Insert into DB with Timestamp
            self.SaveDataintoDB(strNum: data[0] as! NSNumber)
            
            self.lblNumber.text="Got Number: "+String(describing: data[0] as! NSNumber)
            
            
        }
        socket.onAny{print("got event: \($0.event) with items \($0.items)")}
        
        
        socket.connect()
        
    }
    
    func SaveDataintoDB(strNum: NSNumber)
    {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        let randomnumbers = RandomNumbers(context: context)
        
        randomnumbers.number = Int16(strNum)
        randomnumbers.date = NSDate()
        // Save the data to coredata
        
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
    }
    

func triggerNotification(strNum: NSNumber)
    {
        print("notification will be triggered in one second..Hold on tight")
        let content = UNMutableNotificationContent()
        content.title = "App Name"
        content.subtitle = "Number repeated"
        content.body = "Number is: " + String(describing: strNum)
        
        //content.sound = UNNotificationSound.default()
        content.sound = UNNotificationSound.init(named: "ringing.wav")
        
        // Deliver the notification in five seconds.
        let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval:3.0, repeats: false)
        let request = UNNotificationRequest(identifier:requestIdentifier, content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().add(request){(error) in
            
            if (error != nil){
                
                print(error ?? "Error")
            }
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
extension ViewController:UNUserNotificationCenterDelegate{
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        print("Tapped in notification")
    }
    
    //This is key callback to present notification while the app is in foreground
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        print("Notification being triggered")
        
       
        completionHandler( [.alert,.sound,.badge])
        
        //}
    }
}

